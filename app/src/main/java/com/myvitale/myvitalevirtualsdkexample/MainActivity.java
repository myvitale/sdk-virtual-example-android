package com.myvitale.myvitalevirtualsdkexample;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.myvitale.lib.MemberRegistry;
import com.myvitale.lib.SportProfileActivity;
import com.myvitale.lib.VirtualPtActivity;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        MemberRegistry memberRegistry = MemberRegistry.getInstance(
                this.getApplicationContext(),
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                new MemberRegistry.MemberRegistryCallback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "onSuccess: Member is registered successfully");
                    }

                    @Override
                    public void onError(String error) {
                        Log.e(TAG, "onError: Error registering the member: Error: " + error);
                    }
                });
        memberRegistry.register("<EMAIL_TO_REGISTER>");

        Button sportProfileBtn = findViewById(R.id.sportProfileBtn);
        sportProfileBtn.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SportProfileActivity.class);
            startActivity(intent);
        });

        Button virtualTrainingBtn = findViewById(R.id.virtualTrainingBtn);
        virtualTrainingBtn.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, VirtualPtActivity.class);
            startActivity(intent);
        });
    }
}
